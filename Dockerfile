FROM python:2-slim
MAINTAINER Mike Kenney <mikek@apl.uw.edu>

# Setup access to local APT repo
RUN apt-get update && apt-get -y install curl
COPY local.list /etc/apt/sources.list.d/
RUN curl -sS http://wavelet.apl.uw.edu/~mike/public/localrepo.asc | \
      apt-key add - && \
      curl -sS http://wavelet.apl.uw.edu/~mike/public/localpkg.asc |\
      apt-key add -

RUN apt-get update && apt-get -y install \
        jq \
        lrzsz=0.12.21-7mfk1 && \
        apt-get -y autoremove && \
        apt-get clean && \
        mkdir -p /var/log/mlf2

WORKDIR /
COPY requirements.txt /
RUN pip install -r /requirements.txt

COPY apps/* /usr/bin/
COPY entrypoint.sh /
COPY credentials /.mqcreds

ENV DB_SERVER http://mlf2data.apl.uw.edu:5984
ENV AMQP_SERVER mlf2data.apl.uw.edu
ENV MQCREDS /.mqcreds

VOLUME ["/var/log/mlf2"]
ENTRYPOINT ["/entrypoint.sh"]
