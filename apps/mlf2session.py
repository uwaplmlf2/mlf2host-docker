#!/usr/bin/env python
#
# Manage an MLF2 communication session
#
"""
%prog [options] floatid

Manage a communications session for an MLF2 float.
"""
import pika
import os
import sys
import subprocess
import time
import logging
try:
    import simplejson as json
except ImportError:
    import json
import re
from optparse import OptionParser
from contextlib import contextmanager
from select import select


BP = pika.BasicProperties
XMODEM_RECV = 'rx -c -b -O %(filename)s'


class SessionTimeout(Exception):
    pass


@contextmanager
def pushd(dirname):
    """
    Context manager to temporarily work in a new directory
    """
    cwd = os.getcwd()
    os.chdir(dirname)
    try:
        yield
    finally:
        os.chdir(cwd)


def timestamp():
    return long(time.time() * 1000000L)


def mq_setup(host, float_id, credentials, data_queues=False):
    creds = pika.PlainCredentials(*credentials)
    params = pika.ConnectionParameters(host=host,
                                       virtual_host='mlf2',
                                       credentials=creds)
    conn = pika.BlockingConnection(params)
    channel = conn.channel()
    channel.exchange_declare(exchange='commands',
                             type='topic',
                             durable=True)

    for name in ('commands', 'cancellation'):
        qname = 'float-%d.%s' % (float_id, name)
        channel.queue_declare(queue=qname,
                              durable=True,
                              exclusive=False,
                              auto_delete=False)
        channel.queue_bind(exchange='commands',
                           queue=qname,
                           routing_key=qname)

    channel.exchange_declare(exchange='data',
                             type='topic',
                             durable=True)
    if data_queues:
        for name in ('status', 'file'):
            qname = 'float.%s' % (name,)
            channel.queue_declare(queue=qname,
                                  durable=True,
                                  exclusive=False,
                                  auto_delete=False)
            channel.queue_bind(exchange='data',
                               queue=qname,
                               routing_key='*.'+name)

    return conn, channel


def get_line(f_in, timeout=30, eol='\n'):
    """
    Get the next line from the input
    """
    fd = f_in.fileno()
    buf = []
    rlist, _, _ = select([f_in], [], [], timeout)
    while rlist:
        c = os.read(fd, 1)
        if c == eol:
            return ''.join(buf).strip()
        buf.append(c)
        rlist, _, _ = select([f_in], [], [], timeout)
    raise SessionTimeout


def ask(f_in, f_out, prompt, timeout=30, eol='\n'):
    """
    Issue a prompt and wait for a reply or timeout. Returns the reply string
    with the EOL character removed or None if a timeout occurs.
    """
    os.write(f_out.fileno(), prompt)
    return get_line(f_in, timeout=timeout, eol=eol)


def get_status(f_in, timeout=30):
    mimetype = get_line(f_in, timeout=timeout)
    contents = get_line(f_in, timeout=timeout)
    return mimetype, contents


def get_file(f_in, f_out, filename, workdir, newfilename=None):
    cmdline = XMODEM_RECV % {'filename': filename}
    with pushd(workdir):
        logging.info('Exec: %r', cmdline)
        retval = subprocess.call(cmdline.split(), stdin=f_in, stdout=f_out,
                                 stderr=f_out)
        if retval == 0:
            if newfilename:
                os.rename(filename, newfilename)
                filename = newfilename
            return os.path.join(workdir, filename)
        else:
            logging.warning('File download failed')
            os.unlink(filename)
            return None


def check_cancellation(channel, float_id):
    """
    Get all IDs of cancelled commands.
    """
    ids = dict()
    qname = 'float-%d.cancellation' % float_id
    method_frame, header_frame, body = channel.basic_get(queue=qname,
                                                         no_ack=True)
    while method_frame and method_frame.NAME != 'Basic.GetEmpty':
        for val in json.loads(body):
            ids[val] = 1
        method_frame, header_frame, body = channel.basic_get(queue=qname,
                                                             no_ack=True)
    return ids


def send_commands(f_in, f_out, conn, channel, float_id, timeout=60):
    """
    Send each command from the message-queue in turn and wait for a linefeed
    terminated response. If a response is received, an ACK is sent to the
    queue and the command is deleted.

    Responses are published to 'float-N.responses'
    """
    incoming = 'float-%d.commands' % float_id
    outgoing = 'float-%d.responses' % float_id
    cancellations = check_cancellation(channel, float_id)
    logging.info('Found %d cancellation messages', len(cancellations))
    while conn.is_open:
        method_frame, header_frame, body = channel.basic_get(queue=incoming,
                                                             no_ack=False)
        if method_frame is None or method_frame.NAME == 'Basic.GetEmpty':
            # Message queue is empty. Send a blank line to
            # indicate that we are done. Restore any remaining
            # cancellation IDs to the queue
            f_out.write('\n')
            leftovers = [k for k in cancellations.iterkeys()
                         if cancellations[k] == 1]
            if leftovers:
                channel.basic_publish(exchange='commands',
                                      routing_key='float-%d.cancellation' % float_id,
                                      body=json.dumps(leftovers),
                                      properties=BP(delivery_mode=2,
                                                    timestamp=timestamp(),
                                                    content_type='application/json'))

            break
        # Check if this command has been cancelled
        db_id = header_frame.headers.get('db_id')
        if db_id in cancellations:
            logging.info('Cancelling message %s', db_id)
            channel.basic_ack(delivery_tag=method_frame.delivery_tag)
            cancellations[db_id] += 1
            continue
        # Send the command and wait for the response
        command = body.strip()
        logging.debug('F%d <- %r', float_id, command)
        if command.startswith('send-file') or command.startswith('get-errors'):
            limit = 120
        else:
            limit = timeout
        try:
            response = ask(f_in, f_out, command+'\n', timeout=limit)
        except SessionTimeout:
            channel.basic_reject(delivery_tag=method_frame.delivery_tag,
                                 requeue=True)
            raise
        output = json.dumps({'q': command, 'a': response})
        channel.basic_ack(delivery_tag=method_frame.delivery_tag)
        header_frame.headers['floatid'] = float_id
        header_frame.headers['imei'] = os.environ.get('IRSN')
        channel.basic_publish(exchange='commands',
                              routing_key=outgoing,
                              body=output,
                              properties=BP(delivery_mode=2,
                                            timestamp=timestamp(),
                                            reply_to=header_frame.reply_to,
                                            content_type='application/json',
                                            headers=header_frame.headers))


def guess_type(filename):
    """
    Try to guess the mime-type and encoding (compression) of the named
    file. Returns a tuple of mimetype, encoding, and file extension for
    the uncompressed version of the file (if applicable).

    >>> guess_type('syslog01.txt')
    ('text/x-syslog', None, None)
    >>> guess_type('syslog01.z')
    ('text/x-syslog', 'gzip', '.txt')
    >>> guess_type('syslog0.z')
    ('text/x-syslog', 'gzip', '.txt')
    >>> guess_type('syslog0.txt')
    ('text/x-syslog', None, None)
    >>> guess_type('00index.z')
    ('text/x-listing', 'gzip', '.txt')

    """
    table = ((r'.*\.ncz', ('application/x-netcdf', 'gzip', '.nc')),
             (r'.*\.nc$', ('application/x-netcdf', None, None)),
             (r'syslog\d*\.txt|errors\.txt', ('text/x-syslog', None, None)),
             (r'.*\.txt', ('text/plain', None, None)),
             (r'.*\.sx$', ('text/x-sexp', None, None)),
             (r'.*\.sxz', ('text/x-sexp', 'gzip', '.sx')),
             (r'.*\.csv', ('text/csv', None, None)),
             (r'.*\.xml', ('application/xml', None, None)),
             (r'trk\d*\.z|eng\d*\.z|alt\d*\.z', ('text/csv', 'gzip', '.csv')),
             (r'syslog\d*\.z|errors\.z', ('text/x-syslog', 'gzip', '.txt')),
             (r'.*\.jpg', ('image/jpeg', None, None)),
             (r'00index\.z', ('text/x-listing', 'gzip', '.txt')),
             (r'00index', ('text/x-listing', None, None)),
             (r'.*\.tgz', ('application/x-tar', None, None)),
             (r'.*\.zip', ('application/zip', None, None)),
             (r'.*\.z', ('text/plain', 'gzip', '.txt')),
             (r'.*\.ntz', ('application/octet-stream', 'gzip', '.bin')),
             (r'.*\.jsz', ('application/json', 'gzip', '.json')),
             (r'.*\.jsn', ('application/json', None, 'json')))

    for pattern, result in table:
        if re.match(pattern, filename, re.IGNORECASE):
            return result
    return 'application/octet-stream', None, None


def run_session(f_in, f_out, conn, channel, float_id):
    def quick_look_filename(infile):
        _, ext = os.path.splitext(infile)
        return 'ql-' + time.strftime('%Y%m%d_%H%M%S') + ext
    workdir = os.path.join(os.environ.get('TMPDIR', '/var/tmp'), str(float_id))
    if not os.path.isdir(workdir):
        os.makedirs(workdir)
    headers = {'imei': os.environ.get('IRSN'),
               'floatid': float_id}
    done = False
    while not done:
        response = ask(f_in, f_out, '?', timeout=90)
        logging.debug('F%d -> %r', float_id, response)
        if response == 'status':
            mimetype, contents = get_status(f_in, timeout=90)
            channel.basic_publish(exchange='data',
                                  routing_key='float.status',
                                  body=contents,
                                  properties=BP(delivery_mode=2,
                                                headers=headers,
                                                timestamp=timestamp(),
                                                content_type=mimetype,
                                                content_encoding='us-ascii'))
        elif response.startswith('put '):
            _, filename = response.split()
            # Give the quick-look files a unique filename
            newfilename = None
            for prefix in ('telem', 'ql.', 'dataql.'):
                if filename.startswith(prefix):
                    newfilename = quick_look_filename(filename)
                    break
            pathname = get_file(f_in, f_out, filename, workdir, newfilename)
            if pathname is not None:
                filename = os.path.basename(pathname)
                mimetype, enc, ext = guess_type(filename)
                logging.debug('New file: %s;%s', filename, mimetype)
                if enc == 'gzip':
                    # Determine the name of the uncompressed file
                    base, _ = os.path.splitext(filename)
                    filename = base + ext
                headers['filename'] = filename
                channel.basic_publish(exchange='data',
                                      routing_key='float.file',
                                      body=open(pathname, 'rb').read(),
                                      properties=BP(delivery_mode=2,
                                                    headers=headers,
                                                    timestamp=timestamp(),
                                                    content_type=mimetype,
                                                    content_encoding=enc))
                os.unlink(pathname)
        elif response == 'msgs?':
            send_commands(f_in, f_out, conn, channel, float_id)
        elif response == 'bye':
            done = True


def main():
    parser = OptionParser(usage=__doc__)
    parser.set_defaults(server='localhost', credentials=None,
                        imei=None, verbose=False)
    parser.add_option('-s', '--server',
                      type='string',
                      dest='server',
                      metavar='HOST',
                      help='specify AMQP server host (default: %default)')
    parser.add_option('-c', '--credentials',
                      type='string',
                      dest='credentials',
                      metavar='FILENAME',
                      help='credentials file for server access')
    parser.add_option('-i', '--imei',
                      type='string',
                      dest='imei',
                      help='specify the float\'s Iridium IMEI number')
    parser.add_option('-v', '--verbose',
                      action='store_true',
                      dest='verbose',
                      help='enable more verbose logging')

    opts, args = parser.parse_args()
    if len(args) < 1:
        parser.error('Missing float ID')

    logging.basicConfig(level=(opts.verbose and logging.DEBUG or logging.INFO),
                        format='%(asctime)s %(levelname)-8s %(message)s',
                        datefmt='%Y/%m/%d %H:%M:%S')
    float_id = int(args[0])
    if opts.credentials:
        if os.path.exists(opts.credentials):
            credentials = open(opts.credentials,
                               'r').readline().strip().split(':')
        else:
            credentials = opts.credentials.split(':')
    else:
        credentials = 'guest', 'guest'

    if opts.imei:
        os.environ['IRSN'] = opts.imei

    conn, channel = mq_setup(opts.server, float_id, credentials,
                             data_queues=True)
    try:
        run_session(sys.stdin, sys.stdout, conn, channel, float_id)
    except SessionTimeout:
        logging.warning('Session timed-out\n')
    except Exception:
        logging.exception('Uncaught exception')
    finally:
        channel.close()
        conn.close()


if __name__ == '__main__':
    main()
