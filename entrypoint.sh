#!/bin/bash
#


# On a RUDICS login, the IRSN environment variable will be set to the Iridium
# Serial Number (aka IMEI) of the float.
if [[ -n "$IRSN" ]]; then
    exec 2> /var/log/mlf2/session.log
    DB="mlf2db"
    VIEW="_design/status/_view/imei"

    # Look-up the float-ID associated with this Iridium Serial Number
    if [[ -z "$FLOATID" ]]; then
	    FLOATID=$(curl -s -X GET \
	                   "$DB_SERVER/$DB/$VIEW?key=%22${IRSN}%22&limit=1" |\
                         jq -c '.rows[0].value.floatid')
    fi

    if [[ -n "$FLOATID" ]]; then
	    # Disable character echo to reduce latency and disable the conversion of
	    # carriage-returns to line-feeds
	    stty -echo -icrnl
	    # Run the shell
	    exec mlf2session.py ${DEBUG_SESSION:+-v} -c $MQCREDS -i $IRSN \
	     -s $AMQP_SERVER $FLOATID 2> /var/log/mlf2/session-f${FLOATID}.log
	else
	    echo "Cannot determine float-ID for $IRSN" 1>&2
    fi
else
    exec "$@"
fi
